﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace TP2_REA
{
    public partial class Form2 : Form
    {
        //Déclarations des attributs globales
        private Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private UTF8Encoding utf8 = new UTF8Encoding();
        private Form1 form1;
        private byte[] buffer;
        System.Media.SoundPlayer message = new System.Media.SoundPlayer(Properties.Resources.msg);

        public Form2(Form1 form1)
        {
            this.form1 = form1;
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //Vérifie si c'est le serveur ou le client qui a démarré le chat
            if (form1.isServer)
            {
                socket = form1.socketClientTCP;
            }
            else
            {
                socket = form1.socketServerTCP;
            }
            //Tableau de 1024 octets qui permettra la réception des paquets
            buffer = new byte[1024];
            try
            {
                //Début de l'écoute du socket en asynchrone
                socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), buffer);
            }
            catch (Exception ex)
            {
                writeLine(ex.ToString());
            }
        }

        //Réception asynchrone du tcp
        private void ReceiveCallBack(IAsyncResult ar)
        {
            //Tableau des données
            byte[] receiveData = new byte[1024];
            receiveData = (byte[])ar.AsyncState;
            //Trim les données 'vides' et convertie en UTF-8
            string receivedMessage = utf8.GetString(receiveData).Trim('\0');

            //Si le partenaire veut fermer la discussion
            if (receivedMessage.StartsWith("[CLOSE]"))
            {
                this.Close();
            }
            //Sinon, c'est un message
            else
            {
                writeLine(receivedMessage);
                message.Play();
            }

            //Redémarrage de l'écoute, car le message a été traité en haut ^
            buffer = new byte[1024];
            try
            {
                socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), buffer);
            }
            catch
            {
                writeLine("(SYSTEM) ReceiveCallBack error.");
            }
        }

        //Écrit le message 'str', puis défile le contrôle tout en bas
        private void writeLine(string str)
        {
            if (OutputTextBox.TextLength > 0)
            {
                OutputTextBox.Text += "\r\n\r\n" + str;
            }
            else
            {
                OutputTextBox.Text += str;
            }
            OutputTextBox.SelectionStart = OutputTextBox.Text.Length;
            OutputTextBox.ScrollToCaret();
        }

        //Routine d'envoi de messages du socket TCP en cours
        private void sendMessage(string msg)
        {
            //Le message sera placé dans un tableau de 1024 octets
            byte[] envoiData = new byte[1024];
            //Le message sera convertie en UTF8 sous la forme (NOM:ENTRÉE)
            envoiData = utf8.GetBytes(msg);
            try
            {
                //Envoi du message sur le socket TCP
                //envoiData = le message
                //envoiData.Length = la longueur du message pour vérifier son intégrité à la réception
                //SocketFlags.None = aucun flag
                socket.Send(envoiData, envoiData.Length, SocketFlags.None);
                //Affiche le message envoyé
                writeLine(form1.me.username + ":" + InputTextBox.Text);
            }
            catch
            {
                writeLine("(SYSTEM) Unable to send the message.");
            }
            //Vide le contrôle 'InputTextBox' car le message a été envoyé :-)
            InputTextBox.Clear();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Envoi du message de fermeture au partenaire
            sendMessage("[CLOSE]");
            //Arrêt de la réception et de l'envoi du socket
            try
            {
                socket.Shutdown(SocketShutdown.Both);
            }
            catch
            {
                writeLine("(SYSTEM) Socket can't shutdown properly.");
            }
            //Fermeture du socket
            try
            {
                socket.Close();
            }
            catch
            {
                writeLine("(SYSTEM) Socket can't shutdown properly.");
            }
        }

        //---------------- INPUT ----------------
        private void SendButton_Click(object sender, EventArgs e)
        {
            //Valide que l'entrée n'est pas vide
            if (InputTextBox.Text.Length > 0)
            {
                sendMessage(form1.me.username + ":" + InputTextBox.Text); // <-- Envoi le message
            }
        }

        private void InputTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Valide que l'entrée n'est pas vide
            if (InputTextBox.Text.Length > 0)
            {
                //Valide que la touche CTRL+ENTER ne sont pas enfoncés en même temps (Permet de sauter de ligne)
                if (e.KeyChar == Convert.ToChar(Keys.Return) && e.KeyChar != Convert.ToChar(Keys.ControlKey))
                {
                    sendMessage(form1.me.username + ":" + InputTextBox.Text); // <-- Envoi le message
                    e.Handled = true;
                }
            }
            else if (e.KeyChar == Convert.ToChar(Keys.Return))
            {
                e.Handled = true;
            }
        }
    }
}
