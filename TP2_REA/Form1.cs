﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP2_REA
{
    public partial class Form1 : Form
    {
        //Création de la classe 'Users'.
        //Permet une association NOM:IP
        public class Users
        {
            public string username;
            public string IP;
        }

        //Déclarations des attributs globales
        private string version;
        private string name;
        private Socket socketListen, socketSend;
        private EndPoint endpointListen, endpointSend, endpointExchange;
        private byte[] buffer;
        private IPAddress[] localIP;
        private UInt16 udpPORT = 6969;
        public Users me = new Users();
        private static List<Users> usersList = new List<Users>();
        private UTF8Encoding utf8 = new UTF8Encoding();
        private bool isListening = false;
        private bool isConnected = false;
        private bool hasBeenKicked = false;
        //Charge les ressources audios en mémoire
        System.Media.SoundPlayer join = new System.Media.SoundPlayer(Properties.Resources.join);
        System.Media.SoundPlayer quit = new System.Media.SoundPlayer(Properties.Resources.quit);
        System.Media.SoundPlayer message = new System.Media.SoundPlayer(Properties.Resources.msg);
        System.Media.SoundPlayer error = new System.Media.SoundPlayer(Properties.Resources.error);
        public bool isServer;
        private Users friend = new Users();
        public Socket socketServerTCP, socketClientTCP;
        private Socket socketListenTCP;
        private EndPoint epLocal, epDistant;
        private UInt16 tcpPORT;

        public Form1()
        {
            InitializeComponent();
            //Obtient la version du programme
            version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
            //Obtient le nom du programme
            name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Paramètres par défaut lors du démarrage
            Text = name + " - v" + version;
            NameTextBox.Font = new Font(NameTextBox.Font, FontStyle.Regular);
            NameTextBox.ForeColor = Color.Black;
            //Ajoute un pseudonyme aléatoire dans le controle 'NameTextBox'
            Random random = new Random();
            NameTextBox.Text = "user" + random.Next(10000).ToString();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Déconnecte proprement lors de la fermeture
            disconnect();
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            ConnectButton.Enabled = false;
            NameTextBox.Enabled = false;
            //Si n'est pas connecté, démarre la routine de préconnexion
            if (!isConnected)
            {
                //Valide que le pseudonyme n'est pas vide, sinon affiche un message d'erreur
                if (!NameTextBox.Font.Italic && NameTextBox.Text != null)
                {
                    //Remplace tous les espaces ' ' par '_' du 'NameTextBox'
                    string formattedUsername = NameTextBox.Text;
                    formattedUsername = formattedUsername.Replace(" ", "_");
                    NameTextBox.Text = formattedUsername;
                    //Démarre la préconnexion
                    preConnect();
                }
                else
                {
                    MessageBox.Show("Please enter your pseudonym.",
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                    ConnectButton.Enabled = true;
                    NameTextBox.Enabled = true;
                }
            }
            //Sinon déconnecte
            else
            {
                disconnect();
            }
        }

        private void CheckBoxTopMost_CheckedChanged(object sender, EventArgs e)
        {
            //Change l'état 'TopMost' de la form
            TopMost = !TopMost;
            CheckBoxTopMost.Checked = TopMost;
        }

        //---------------- INPUT ----------------
        private void InputTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Valide que l'entrée n'est pas vide
            if (InputTextBox.Text.Length > 0)
            {
                //Valide que la touche CTRL+ENTER ne sont pas enfoncés en même temps (Permet de sauter de ligne)
                if (e.KeyChar == Convert.ToChar(Keys.Return) && e.KeyChar != Convert.ToChar(Keys.ControlKey))
                {
                    sendMessage(); // <-- Envoi le message
                    e.Handled = true;
                }
            }
            else if (e.KeyChar == Convert.ToChar(Keys.Return))
            {
                e.Handled = true;
            }
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            //Valide que l'entrée n'est pas vide
            if (InputTextBox.Text.Length > 0)
            {
                sendMessage(); // <-- Envoi le message
            }
        }

        //---------------- NAMETEXTBOX ----------------
        private void NameTextBox_Enter(object sender, EventArgs e)
        {
            //Change l'aspect de 'NameTextBox' lorsque sélectionné
            if (NameTextBox.Font.Italic)
            {
                NameTextBox.Font = new Font(NameTextBox.Font, FontStyle.Regular);
                NameTextBox.ForeColor = Color.Black;
                NameTextBox.Text = null;
            }
        }

        private void NameTextBox_Leave(object sender, EventArgs e)
        {
            //Remet le contrôle 'NameTextBox' à sont état d'origine lorsque déselectionné
            if (NameTextBox.Text == "")
            {
                NameTextBox.Font = new Font(NameTextBox.Font, FontStyle.Italic);
                NameTextBox.ForeColor = Color.DarkGray;
                NameTextBox.Text = "username...";
            }
        }

        //---------------- TIMER ----------------
        private void timer1_Tick(object sender, EventArgs e)
        {
            //Fait une demandeInfo en broadcast à tous les 2000ms
            demandeInfo();
            toolStripStatusLabel1.Text = "Clients connected: " + (GroupListBox.Items.Count+1);
        }

        //---------------- NEW INSTANCE ----------------
        private void NewInstanceButton_Click(object sender, EventArgs e)
        {
            //Permet de créer une nouvelle instance du programme
            var process = Process.GetCurrentProcess();
            string fullPath = process.MainModule.FileName;
            Process.Start(fullPath);
        }

        //Affiche le message de débogage 'str', puis défile le contrôle tout en bas
        private void print(string str)
        {
            if (LogsTextBox.TextLength > 0)
            {
                LogsTextBox.Text += "\r\n" + str;
            } else {
                LogsTextBox.Text += str;
            }
            LogsTextBox.SelectionStart = LogsTextBox.Text.Length;
            LogsTextBox.ScrollToCaret();
        }

        //Écrit le message 'str', puis défile le contrôle tout en bas
        private void writeLine(string str)
        {
            if (OutputTextBox.TextLength > 0)
            {
                OutputTextBox.Text += "\r\n\r\n" + str;
            } else {
                OutputTextBox.Text += str;
            }
            OutputTextBox.SelectionStart = OutputTextBox.Text.Length;
            OutputTextBox.ScrollToCaret();
        }

        //Réception asynchrone du broadcast
        private void callBackBroadcastEcoute(IAsyncResult ar)
        {
            Invoke((MethodInvoker)delegate
            {
                //Vérifie les données reçues
                try
                {
                    //Tableau des données
                    byte[] receiveData = new byte[1024];
                    receiveData = (byte[])ar.AsyncState;
                    //Trim les données 'vides' et convertie en UTF-8
                    string receivedMessage = utf8.GetString(receiveData).Trim('\0');
                    //Split les chaînes dans un tableau à chaque ':'
                    string[] splittedMessage = receivedMessage.Split(':');
                    Users user = new Users();
                    //Le 'username' est à l'index 1 du tableau
                    user.username = splittedMessage[1];

                    //Si c'est quelqu'un qui donne son "NOM"
                    if (receivedMessage.StartsWith("[NAME]:"))
                    {
                        user.IP = splittedMessage[2];
                        if (!(usersList.Exists(x => x.username == user.username))) // <-- Si ce n'est pas moi-même
                        {
                            usersList.Add(new Users() { username = user.username, IP = user.IP });
                            print(user.username+":"+user.IP);
                        }
                    }
                    //Si c'est quelqu'un qui veux savoir "QUI" on est
                    else if (receivedMessage.StartsWith("[WHO]:"))
                    {
                        //Répondre à cette requête SEULEMENT si on est connecté
                        if (isConnected)
                        {
                            envoiInfo("[NAME]");
                        }
                    }
                    //Si c'est quelqu'un qui dit qui décriss
                    else if (receivedMessage.StartsWith("[QUIT]:"))
                    {
                        quit.Play();
                        writeLine("(SYSTEM) " + user.username + " left the chat.");
                    }
                    //Si c'est quelqu'un qui dit qui vien d'arriver
                    else if (receivedMessage.StartsWith("[JOIN]:"))
                    {
                        join.Play();
                        writeLine("(SYSTEM) " + user.username + " joined the chat.");
                    }
                    //Si c'est quelqu'un qui dit qui veut parler privé
                    else if (receivedMessage.StartsWith("[PRIVATE]:"))
                    {
                        //Affiche la Form2 en se connectant au 'sckServeur'
                        showPrivateChat(splittedMessage[1], splittedMessage[2], splittedMessage[3]);
                    }
                    //Sinon, c'est un message!
                    else
                    {
                        message.Play();
                        writeLine(receivedMessage);
                    }

                    //Redémarrage de l'écoute, car le message a été traité en haut ^
                    buffer = new byte[1024];
                    if (isListening)
                    {
                        socketListen.BeginReceiveMessageFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endpointListen, new AsyncCallback(callBackBroadcastEcoute), buffer);
                    }
                }
                catch (Exception ex)
                {
                    print("callBackBroadcastEcoute() " + ex.ToString());
                }
            });
        }

        //Routine pour demander qui est présent sur le chat ET pour inscrire les utilisateurs actuellement connectés
        private void demandeInfo()
        {
            //Envoi la demande 'Y'a tu quelqu'un icitttte?' sur le broadcast
            envoiInfo("[WHO]");
            //Vide la belle p'tite liste dans 'GroupListBox'
            GroupListBox.Items.Clear();
            GroupListBox.BeginUpdate();
            foreach (Users user in usersList)
            {
                if (user != null)
                {
                    //Valide que le 'username' qui nous représente ne soit pas ajouté dans la 'GroupListBox'
                    if (!(String.Compare(me.username, user.username, false) == 0))
                    {
                        GroupListBox.Items.Add(user.username);
                    }
                }
                //Si n'est pas connecté (donc est en préconnexion), botter le cul à l'utilisateur
                //qui essai de se faire passer pour quelqu'un d'autre, sinon se connecter!!!
                if (!isConnected)
                {
                    if (user.username == me.username)
                    {
                        hasBeenKicked = true;
                        disconnect();
                        error.Play();
                        writeLine("(SYSTEM) Failed to connect. Someone have the same username.");
                    }
                    else
                    {
                        connect();
                    }
                }
            }
            //Si c'est le premier qui se connecte au sous-réseau, bah pourquoi l'empêcher de se connecter?
            if (usersList.Count == 0)
            {
                connect();
            }
            GroupListBox.EndUpdate();

            //Vide la liste d'utilisateurs pour qu'elle soit prête à la prochaine réception asynchrone du broadcast
            usersList.Clear();
        }

        //Envoi d'un message sur le broadcast selon la commande 'command'
        private void envoiInfo(string command)
        {
            //Le message est un tableau de 1024 octets max
            byte[] sendingMessage = new byte[1024];
            //Conversion du message en UTF8 sous la forme (CODE:NOM:IP)
            sendingMessage = utf8.GetBytes(command + ":" + me.username + ":" + me.IP);
            //Envoi du message sur le socket
            try
            {
                socketSend.Send(sendingMessage);
            }
            catch
            {
                print("System: envoiInfo("+command+") has encountered a problem.");
            }
        }

        //Assigne le pseudonyme et l'adresse IP par défaut de la 'Form' actuelle
        private void set_me()
        {
            me.username = NameTextBox.Text;
            print("Me: " + me.username);
            localIP = Dns.GetHostAddresses(Dns.GetHostName());
            //<Attention>, ici le dernier IP de la liste sera assigné par défaut. Si tu as trop de carte réseau, too bad mon ami
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    me.IP = address.ToString();
                    print("IPv4: " + me.IP);
                }
                else
                {
                    print("IPv6: " + address.ToString());
                }
            }
        }

        //Routine de préconnexion (Permet de vérifier d'abord qui est présent sur le chat)
        private void preConnect()
        {
            //Vide la liste d'utilisateurs
            usersList.Clear();

            //Assigne le pseudonyme et l'adresse IP par défaut de la 'Form' actuelle
            set_me();

            //Définition des sockets (InterNetwork = IPv4, Dgram = Datagramme, Udp = Protocole UDP)
            socketListen = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socketSend = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //Configuration des sockets (Socket = Tous les sockets, ReuseAddress = Adresse actif, Broadcast = L'adresse de broadcast)
            socketListen.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            socketSend.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);

            //Configuration du endpoint (Any = Toutes les adresses IP, udpPORT = Sur le port 6969)
            endpointListen = new IPEndPoint(IPAddress.Any, udpPORT);
            //Join le socket d'écoute au endpoint
            socketListen.Bind(endpointListen);

            //Tableau de 1024 octets qui permettra la réception des paquets
            buffer = new byte[1024];

            //Permet au socketListen de se déconnecter proprement lorsqu'il est asynchrone
            isListening = true;

            hasBeenKicked = false;

            //Début de l'écoute de socketListen
            try
            {
                socketListen.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endpointListen, new AsyncCallback(callBackBroadcastEcoute), buffer);
            }
            catch (Exception ex)
            {
                print(ex.ToString());
            }

            //Configuration du endpoint (Broadcast = L'adresse IP broadcast seulement, udpPORT = Sur le port 6969)
            endpointSend = new IPEndPoint(IPAddress.Broadcast, udpPORT);
            //Join le socket d'envoi au endpoint
            socketSend.Connect(endpointSend);
            //Démarre le timer (Tous les 2000ms, il appelera la routine 'demandeInfo()')
            timer1.Start();
        }

        //Routine de connexion (Permet de se 'connecter' officiellement au chat)
        private void connect()
        {
            isConnected = true;
            ConnectButton.Enabled = true;
            ConnectButton.Image = Properties.Resources.plug_connect_icon;
            NameTextBox.Enabled = false;
            GroupListBox.Enabled = true;
            splitContainer1.Enabled = true;
            toolStripStatusLabel1.Text = "Connected";
            //Envoi un signal sur le broadcast que l'utilisateur en cours vien d'entré dans le chat
            envoiInfo("[JOIN]");

            print("System: Connected.");
        }

        //Routine de déconnexion (Permet une déconnexion propre de tous les sockets)
        private void disconnect()
        {
            try
            {
                isConnected = false;
                ConnectButton.Enabled = true;
                ConnectButton.Image = Properties.Resources.plug_disconnect_icon;
                NameTextBox.Enabled = true;
                GroupListBox.Enabled = false;
                splitContainer1.Enabled = false;
                GroupListBox.Items.Clear();
                toolStripStatusLabel1.Text = "Disconnected";
                timer1.Stop();
                isListening = false;
                //Envoi un signal sur le broadcast que l'utilisateur en cours vien de quitter le chat SEULEMENT si
                //l'utilisateur a été exclu
                if (!hasBeenKicked)
                {
                    envoiInfo("[QUIT]");
                }
                //Désactive les sockets (Both = Reçu/Envoyé)
                socketListen.Shutdown(SocketShutdown.Both);
                socketSend.Shutdown(SocketShutdown.Both);
                //Ferme (dispose de ses ressources) officiellement les sockets
                socketListen.Close();
                socketSend.Close();

                print("System: Disconnected.");
            }
            catch
            {
                print("System: disconnect() has encountered a problem.");
            }
        }

        //Routine d'envoi de messages UDP au sous-réseau
        private void sendMessage()
        {
            //Le message sera placé dans un tableau de 1024 octets
            byte[] msg = new byte[1024];
            //Le message sera convertie en UTF8 sous la forme (NOM:ENTRÉE)
            msg = utf8.GetBytes(me.username + ":" + InputTextBox.Text);
            try
            {
                //Pour chaque utilisateur dans la liste d'utilisateurs envoyer le message sur
                //leur adresse IP respectif, puis remettre le endpoint initiale
                foreach (Users user in usersList)
                {
                    endpointExchange = new IPEndPoint(IPAddress.Parse(user.IP), udpPORT);
                    socketSend.Connect(endpointExchange);
                    socketSend.Send(msg);
                    socketSend.Connect(endpointSend);
                }                
            }
            catch
            {
                print("System: sendMessage() has encountered a problem.");
            }
            //Vide le contrôle 'InputTextBox' car le message a été envoyé :-)
            InputTextBox.Clear();
        }

        /*=======================================================================================================
          ============================================= PARTIE TCP ==============================================
          =======================================================================================================*/

        //Action lorsque l'item est double cliqué
        private void GroupListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            friend = new Users();
            try
            {   //Choix d'un utilisateur dans la 'GroupListBox'
                friend.username = GroupListBox.SelectedItem.ToString();
                //Lancement de l'écoute
                listenTCP();
            }
            catch
            {
                print("GroupListBox_MouseDoubleClick() has encountered a problem.");
            }
        }

        //Établit la communication en TCP pour la conversation privée avec le demandeur
        //'destIP' l'IP du demandeur, 'destPORT' le port du demandeur, 'destNAME' le pseudonyme du demandeur
        private void showPrivateChat(string destIP, string destPORT, string destNAME)
        {
            //Définition du 'socketServerTCP' (InterNetwork = IPv4, Stream = Flux d'information, Tcp = Protocole TCP)
            socketServerTCP = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //Requête de la connexion. L'IP et le port du demandeur sera envoyé au serveur.
            //Si la requête est acceptée (de son côté), la communication TCP pourra commencer.
            try
            {
                //Création d'un Endpoint distant
                epDistant = new IPEndPoint(IPAddress.Parse(destIP), Convert.ToInt32(destPORT));
                socketServerTCP.Connect(epDistant);
            }
            catch
            {
                print("System: showPrivateChat() has encountered a problem.");
            }

            //Permettra dans la 'Form2' de choisir le bon socket à utiliser pour communiquer
            //'false' car c'est l'AMI qui fait une demande!
            isServer = false;
            //Création d'une référence et d'un objet de type Form2
            Form2 privateChat = new Form2(this);
            //Affiche Form2
            privateChat.Show();
            privateChat.Text = "(" + destNAME + ")";
        }

        //Routine asynchrone pour lancer l'écoute en TCP
        private async void listenTCP()
        {
            //Définition des sockets (InterNetwork = IPv4, Stream = Flux d'information, Tcp = Protocole TCP)
            socketListenTCP = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socketClientTCP = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //Génération d'un entier aléatoire entre 1025 et 65536
            Random random = new Random();
            int port = random.Next(1025, 65536);
            //Convertie le nombre en entier 16bits non signé avant de l'assigner à 'tcpPORT'
            tcpPORT = Convert.ToUInt16(port);
            //Configuration du endpoint (Parse(me.IP) = Mon adresse IP, tcpPORT = Sur le port aléatoire choisit)
            epLocal = new IPEndPoint(IPAddress.Parse(me.IP), tcpPORT);
            //Join le socket d'écoute au endpoint
            socketListenTCP.Bind(epLocal);
            //Met le socket TCP en écoute
            //3 requêtes possibles dans la file, les autres seront rejetés et receveront un message 'serveur busy'
            socketListenTCP.Listen(3);

            print("System: (TCP) Listen started.");
            try
            {
                //Le message sera placé dans un tableau de 1024 octets
                byte[] sendingMessage = new byte[1024];
                //Le message sera convertie en UTF8 sous la forme (PRIVÉE:IP:PORT:NOM)
                sendingMessage = utf8.GetBytes("[PRIVATE]:" + me.IP + ":" + tcpPORT.ToString() + ":" + me.username);

                foreach (Users user in usersList)
                {
                    if (user != null)
                    {
                        if (user.username == friend.username)
                        {
                            //Envoie la demande de chat privée toujours en utilisant le protocol UDP
                            endpointExchange = new IPEndPoint(IPAddress.Parse(user.IP), udpPORT);
                            socketSend.Connect(endpointExchange);
                            socketSend.Send(sendingMessage);
                        }
                    }
                }
            }
            catch
            {
                print("System: listenTCP() has encountered a problem.");
            }

            //Démarre un Task (Task.Factory.StartNew) asynchrone (await) dans lequel on écoute le socket.
            //Lorsque "Accept()" retournera une réponse (un socket),
            //la réponse sera mise dans sckClient (socket du Client(celui qui fait une requête de connexion)).
            //"() => socketListenTCP.Accept())" : méthode exécutée par le task.
            //La valeur retournée, au lieu d'être un task, sera convertie au type de socketClientTCP
            socketClientTCP = await Task.Factory.StartNew(() => socketListenTCP.Accept());
            //Permettra dans la 'Form2' de choisir le bon socket à utiliser pour communiquer
            //'true' car c'est MOI qui fait une demande!
            isServer = true;
            //Création d'une référence et d'un objet de type Form2
            Form2 privateChat = new Form2(this);
            //Affiche Form2
            privateChat.Show();
            privateChat.Text = "(" + friend.username + ")";
        }

    }
}
